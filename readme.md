# ¿Cuántos paneles caben?

## 🎯 Objetivo

El objetivo de este ejercicio es entender tus habilidades como programador/a, la forma en que planteas un problema, cómo lo resuelves y, finalmente, cómo comunicas tu forma de razonar y resultados.

## 🛠️ Problema

El problema a resolver consiste en encontrar la máxima cantidad de rectángulos de dimensiones “a” y “b” (paneles solares) que caben dentro de un rectángulo de dimensiones “x” e “y” (techo).

## 📜 Instrucciones

- Usa el lenguaje/framework que más te acomode. No hay una solución única al problema, por lo que puedes hacer lo que prefieras.
- El algoritmo debe ser una sola función que reciba las dimensiones y retorne un solo entero con la cantidad de paneles que caben.
- No hay restricciones de orientación. Pon todos los rectángulos que puedas en la posición y sentido que prefieras.
- No se pide nada gráfico.

## Introducción

Este repositorio contiene una implementación en Typescript de un algoritmo que calcula la cantidad máxima de paneles solares rectangulares que pueden caber en un techo, dado un conjunto de dimensiones específicas. El algoritmo resuelve problemas para techos rectangulares y triangulares, y considera la posible superposición de paneles.

## 🚀 Ejecución del código

A continuación, se presenta cómo puedes ejecutar el código proporcionado.

### Prerrequisitos

- Asegúrate de tener [Node.js](https://nodejs.org/) instalado en tu máquina.

### Pasos

1. Clona este repositorio.
2. Abre una terminal y navega hasta el directorio raíz.
3. Instala las dependencias

   ```bash
   npm install
   ```

4. Ejecuta las pruebas unitarias con el siguiente comando

   ```bash
   npm run test
   ```
