import {
  calcPanelesEnTecho,
  calcPanelesEnTechoSuperpuesto,
  calcPanelesEnTriangulo,
} from "./formulas";

describe("Calculo de paneles en techo rectangular", () => {
  test("Paneles 1x2 y techo 2x4 caben 4 paneles", () => {
    expect(calcPanelesEnTecho({ x: 1, y: 2 }, { x: 2, y: 4 })).toBe(4);
  });
  test("Paneles 1x2 y techo 3x5 caben 7 paneles", () => {
    expect(calcPanelesEnTecho({ x: 1, y: 2 }, { x: 3, y: 5 })).toBe(7);
  });
  test("Paneles 1x2 y techo 1x10 caben 5 paneles", () => {
    expect(calcPanelesEnTecho({ x: 1, y: 2 }, { x: 1, y: 10 })).toBe(5);
  });
  test("Paneles 2x2 y techo 1x10 no caben paneles", () => {
    expect(calcPanelesEnTecho({ x: 2, y: 2 }, { x: 1, y: 10 })).toBe(0);
  });
});

describe("\nCalculo de paneles en techo Triangular", () => {
  test("Paneles 1x2 y techo base 3 altura 3 cabe 1 panel", () => {
    expect(calcPanelesEnTriangulo({ x: 1, y: 2 }, { b: 2, h: 4 })).toBe(1);
  });

  test("Paneles 1x2 y techo base 8 altura 10 caben 14 paneles", () => {
    expect(calcPanelesEnTriangulo({ x: 1, y: 2 }, { b: 8, h: 10 })).toBe(14);
  });

  test("Paneles 1.25x2.3 y techo base 9 altura 7 caben 6 paneles", () => {
    expect(calcPanelesEnTriangulo({ x: 1.25, y: 2.3 }, { b: 9, h: 7 })).toBe(6);
  });
});

describe("\nCalculo de paneles en techos superpuestos", () => {
  test("Paneles 1x3 y techos 7x12 y superpuestos en 2x3 caben 54 paneles", () => {
    expect(
      calcPanelesEnTechoSuperpuesto(
        { x: 1, y: 3 },
        { x: 7, y: 12 },
        { x: 2, y: 3 }
      )
    ).toBe(54);
  });
  test("Paneles 1x2 y techos 5x4 y superpuestos en 2x2 caben 18 paneles", () => {
    expect(
      calcPanelesEnTechoSuperpuesto(
        { x: 1, y: 2 },
        { x: 5, y: 4 },
        { x: 2, y: 2 }
      )
    ).toBe(18);
  });
});
