import { Rectangulo, Triangulo } from "./defs";

export const calcPanelesEnTecho = (
  panel: Rectangulo,
  techo: Rectangulo
): number => {
  if (
    (panel.x > techo.x || panel.x > techo.y) &&
    (panel.y > techo.y || panel.y > techo.x)
  )
    return 0;

  const panelArea = panel.x * panel.y;
  const techoArea = techo.x * techo.y;
  const cantidadDePaneles = techoArea / panelArea;
  return Math.trunc(cantidadDePaneles);
};

export const calcPanelesEnTriangulo = (
  panel: Rectangulo,
  techo: Triangulo
): number => {
  const paneles: number[] = [];
  const pendiente = -techo.h / techo.b;
  for (let i = panel.y; i < techo.h; i += panel.y) {
    const intersectoHipotenusaY = techo.b + i / pendiente;
    const panelesEnPiso = Math.trunc(intersectoHipotenusaY / panel.x);
    paneles.push(panelesEnPiso);
  }
  return paneles.reduce((a, b) => a + b, 0);
};

export const calcPanelesEnTechoSuperpuesto = (
  panel: Rectangulo,
  techo: Rectangulo,
  superposicion: Rectangulo
) => {
  if (superposicion.x == 0 && superposicion.y == 0)
    return calcPanelesEnTecho(panel, techo);
  const areaTecho = techo.x * techo.y;
  const areaCentral = superposicion.x * superposicion.y;
  const areaTotal = areaTecho * 2 - areaCentral;

  const areaPanel = panel.x * panel.y;

  return Math.trunc(areaTotal / areaPanel);
};
