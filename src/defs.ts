export interface Rectangulo {
  x: number;
  y: number;
}
export interface Triangulo {
  b: number;
  h: number;
}
